package data

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type tableCollection struct {
	Tables map[string]table
}

var tc tableCollection
var jsonFilename = "tables.json"

func GetTC() *tableCollection {
	if tc.Tables == nil {
		tc.Tables = make(map[string]table)
	}
	return &tc
}

func (tc *tableCollection) DefineTable(tableName string, extendedFieldList string) error {
	if tbl, err := NewTable(tableName, extendedFieldList); err != nil {
		return err
	} else {
		if _, ok := tc.Tables[tbl.Name]; ok {
			return fmt.Errorf("Table already defined: %s", tbl.Name)
		}
		tc.Tables[tbl.Name] = tbl
	}
	return nil
}

func (tc *tableCollection) Drop(tableName string) error {
	if tbl, err := tc.GetTable(tableName); err != nil {
		return err
	} else {
		delete(tc.Tables, tbl.Name)
		return nil
	}
}

func (tc *tableCollection) GetTable(tableName string) (table, error) {
	if tbl, ok := tc.Tables[strings.ToUpper(tableName)]; ok {
		return tbl, nil
	} else {
		return tbl, fmt.Errorf("Table not found: %s", strings.ToUpper(tableName))
	}
}

func (tc *tableCollection) Insert(tableName string, valueList string) error {
	if tbl, err := tc.GetTable(tableName); err != nil {
		return err
	} else {
		return tbl.Insert(valueList)
	}
}

func (tc *tableCollection) ReadFromFile() error {
	if _, err := os.Open(jsonFilename); err != nil {
		_, err = os.Create(jsonFilename)
		return err
	} else {
		if jsondata, err := ioutil.ReadFile(jsonFilename); err != nil {
			return err
		} else {
			return json.Unmarshal(jsondata, &tc)
		}
	}
}

func (tc *tableCollection) String() string {
	var tcstring string
	for _, tbl := range tc.Tables {
		tcstring += "\n" + tbl.Name + "\n"
		for _, fld := range tbl.Fields {
			tcstring += fmt.Sprintf("| %s (%s)\n", fld.Name, fld.Type)
		}
	}
	tcstring += "\n<-- End of Dictionary -->\n"
	return tcstring
}

func (tc *tableCollection) WriteToFile() error {
	if jsondata, err := json.MarshalIndent(tc, "", "\t"); err != nil {
		return err
	} else {
		return ioutil.WriteFile("tables.json", jsondata, os.ModeExclusive)
	}
}
