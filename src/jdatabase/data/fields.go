package data

import (
	"encoding/binary"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
)

type Field struct {
	Name     string
	Type     string
	ByteSize int
}

func (fld Field) Write(w io.Writer, value string) error {
	switch fld.Type {
	case "date":
		fallthrough
	case "integer":
		if value, err := strconv.ParseInt(value, 0, 64); err != nil {
			return err
		} else {
			return binary.Write(w, binary.LittleEndian, value)
		}
	case "real":
	if value, err := strconv.ParseFloat(value, 64); err != nil {
		return err
	} else {
		return binary.Write(w, binary.LittleEndian, value)
	}
	case "boolean":
//	if value, err := strconv.ParseBool(value); err != nil {
//		return err
//	} else {
//		_, err := w.Write(value)
//		return err
//	}
	case "varchar":
	fallthrough
	case charsyntax.String():
	_, err := w.Write([]byte(value))
	return err
	}
	return fmt.Errorf("Not written: %s, value")
}

var charsyntax = regexp.MustCompile("char\\((\\d+)\\)")

func NewField(fieldName string, fieldType string) (Field, error) {
	var newField Field
	fieldName = strings.ToLower(fieldName)
	fieldType = strings.ToLower(fieldType)
	switch fieldType {
	case "integer":
		newField = Field{fieldName, fieldType, 8}
	case "date":
		newField = Field{fieldName, fieldType, 8}
	case "real":
		newField = Field{fieldName, fieldType, 8}
	case "varchar":
		newField = Field{fieldName, fieldType, 8}
	case "boolean":
		newField = Field{fieldName, fieldType, 1}
	default:
		if charsyntax.MatchString(fieldType) {
			length, _ := strconv.ParseInt(
				charsyntax.FindAllStringSubmatch(fieldType, -1)[0][1], 0, 0)
			newField = Field{fieldName, fieldType, int(length)}
		} else {
			return newField, fmt.Errorf("Invalid field: %s", fieldType)
		}
	}
	return newField, nil
}
