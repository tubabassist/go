package data

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
)

type table struct {
	Name   string
	Fields []Field
}

func (tbl table) Insert(valueList string) error {
	//construct []byte and IF successful, THEN append []byte to table file
	values := strings.Split(valueList, ",")
	if len(values) != len(tbl.Fields) {
		return fmt.Errorf("Too many values: table %s has %f fields.", tbl.Name, len(tbl.Fields))
	}
	buff := new(bytes.Buffer)
	for i := 0; i < len(tbl.Fields); i++ {
		if err := tbl.Fields[i].Write(buff, values[i]); err != nil {
			return err
		}
	}
	fmt.Print(buff)
	return nil
}

func (tbl table) String() string {
	var tblstr string
	tblstr += "\n" + tbl.Name + "\n"
	for _, fld := range tbl.Fields {
		tblstr += fmt.Sprintf("| %s (%s)\n", fld.Name, fld.Type)
	}
	tblstr += "\n<-- End of Selection -->\n"
	return tblstr
}

func NewTable(tableName string, extendedFieldList string) (table, error) {
	var newTable = table{Name: strings.ToUpper(tableName)}
	var extendedFields = strings.Split(extendedFieldList, ",")

	for _, extendedField := range extendedFields {
		input := strings.Fields(extendedField)
		if len(input) != 2 {
			return newTable, errors.New(
				"Invalid command!  Check syntax in field list.  Type 'help;' for proper syntax.")
		}
		if f, err := NewField(input[0], input[1]); err != nil {
			return newTable, err
		} else {
			newTable.Fields = append(newTable.Fields, f)
		}
	}
	return newTable, nil
}
