package commands

import (
	"fmt"
	"regexp"
)

func newUpdate() *update {
	return &update{
		regexp.MustCompile(
			"(?i)update\\s+(\\S+)\\s+set\\s+(\\S+)\\s*=\\s*(\\S+)(?:\\s+where\\s*(.+))?\\s*;"), nil}
}

type update struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *update) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd update) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println(cmd.matchedInput[0][3])
	if cmd.matchedInput[0][4] != "" {
		fmt.Println(cmd.matchedInput[0][4])
	}
	fmt.Println("This is a correct UPDATE command!")
	return nil
}
