package commands

import (
	"fmt"
	"regexp"
)

func newIntersect() *intersect {
	return &intersect{
		regexp.MustCompile(
			"(?i)intersect\\s+(\\(.+\\)|\\S+)\\s+and\\s+(\\(.+\\)|\\S+)\\s*;"), nil}
}

type intersect struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *intersect) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd intersect) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct INTERSECT command!")
	return nil
}
