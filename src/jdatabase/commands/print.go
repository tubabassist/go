package commands

import (
	"jdatabase/data"
	"fmt"
	"regexp"
	"strings"
)

func newPrint() *printCommand {
	return &printCommand{regexp.MustCompile("(?i)print\\s+(\\S+)\\s*;"), nil}
}

type printCommand struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *printCommand) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd printCommand) Execute() error {
	if strings.ToLower(cmd.matchedInput[0][1]) == "dictionary" {
		fmt.Println(data.GetTC())
	} else {
		if tbl, err := data.GetTC().GetTable(cmd.matchedInput[0][1]); err != nil {
			return err
		} else {
			fmt.Println(tbl)
		}
	}
	return nil
}
