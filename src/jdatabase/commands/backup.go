package commands

import (
	"fmt"
	"regexp"
)

func newBackup() *backup {
	return &backup{regexp.MustCompile("(?i)backup\\s+to\\s*'(.+)'\\s*;"), nil}
}

type backup struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *backup) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd backup) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println("This is a correct BACKUP command!")
	return nil
}
