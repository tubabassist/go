package commands

import (
	"fmt"
	"regexp"
)

func newRename() *rename {
	return &rename{
		regexp.MustCompile("(?i)rename\\s+table\\s+(\\S+)\\s+to\\s+(\\S+)\\s*;"), nil}
}

type rename struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *rename) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd rename) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct RENAME command!")
	return nil
}
