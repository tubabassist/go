package commands

import (
	"jdatabase/data"
	"regexp"
)

func newDrop() *drop {
	return &drop{regexp.MustCompile("(?i)drop\\s+table\\s+(\\S+)\\s*;"), nil}
}

type drop struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *drop) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd drop) Execute() error {
	if err := data.GetTC().Drop(cmd.matchedInput[0][1]); err != nil {
		return err
	}
	return data.GetTC().WriteToFile()
}
