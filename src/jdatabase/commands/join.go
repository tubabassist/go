package commands

import (
	"fmt"
	"regexp"
)

func newJoin() *join {
	return &join{
		regexp.MustCompile("(?i)join\\s+(\\(.+\\)|\\S+)\\s+and\\s+(\\(.+\\)|\\S+)\\s*;"), nil}
}

type join struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *join) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd join) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct JOIN command!")
	return nil
}
