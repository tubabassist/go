package commands

import (
	"fmt"
	"os"
	"regexp"
)

func newExit() exit {
	return exit{regexp.MustCompile("(?i)exit\\s*;")}
}

type exit struct {
	syntax *regexp.Regexp
}

func (cmd exit) Matches(input string) bool {
	return cmd.syntax.MatchString(input)
}

func (cmd exit) Execute() error {
	fmt.Println("<-- Exiting Go Database -->")
	os.Exit(0)
	return nil
}
