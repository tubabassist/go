package commands

import (
	"bufio"
	"fmt"
	"io"
)

type Command interface {
	Matches(input string) bool
	Execute() error
}

var Commands = []Command{newBackup(), newDefineIndex(), newDefineTable(), newDelete(),
	newDrop(), newExit(), newHelp(), newInsert(), newIntersect(), newJoin(), newMinus(),
	newOrder(), newPrint(), newProject(), newReadTable(), newRead(), newRename(),
	newRestore(), newSelect(), newUnion(), newUpdate()}

func Process(input io.Reader) error {
	reader := bufio.NewReader(input)
	for {
		if line, err := reader.ReadString(';'); err != nil {
			if err == io.EOF {
				break
			}
			return err
		} else {
			for _, cmd := range Commands {
				if cmd.Matches(line) {
					return cmd.Execute()
				}
			}
			return fmt.Errorf("Invalid command!  Type 'help;' for proper syntax.")
		}
	}
	return nil
}
