package commands

import (
	"fmt"
	"regexp"
)

func newReadTable() *readTable {
	return &readTable{
		regexp.MustCompile("(?i)define\\s+table\\s+(\\S+)\\s+read\\s+'(.+)'\\s*;"), nil}
}

type readTable struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *readTable) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd readTable) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct READ_TABLE command!")
	return nil
}
