package commands

import (
	"fmt"
	"io/ioutil"
	"path"
	"regexp"
	"runtime"
)

func newHelp() help {
	return help{regexp.MustCompile("(?i)help\\s*;")}
}

type help struct {
	syntax       *regexp.Regexp
}

func (cmd help) Matches(input string) bool {
	return cmd.syntax.MatchString(input)
}

func (cmd help) Execute() error { //Not sure this filepath will work outside IDE
	_, filepath, _, _ := runtime.Caller(1)
	if help, err := ioutil.ReadFile(path.Join(path.Dir(filepath), "grammar.txt")); err != nil {
		return err
	} else {
		fmt.Println(string(help))
	}
	return nil
}
