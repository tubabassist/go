package commands

import (
	"fmt"
	"regexp"
)

func newSelect() *selectCommand {
	return &selectCommand{
		regexp.MustCompile("(?i)select\\s+(\\(.+\\)|\\S+)(?:\\s+where\\s+(.+))?\\s*;"), nil}
}

type selectCommand struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *selectCommand) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd selectCommand) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	if cmd.matchedInput[0][2] != "" {
		fmt.Println(cmd.matchedInput[0][2])
	}
	fmt.Println("This is a correct SELECT command!")
	return nil
}
