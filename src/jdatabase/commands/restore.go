package commands

import (
	"fmt"
	"regexp"
)

func newRestore() *restore {
	return &restore{regexp.MustCompile("(?i)restore\\s+from\\s+'(.+)'\\s*;"), nil}
}

type restore struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *restore) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd restore) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println("This is a correct RESTORE command!")
	return nil
}
