package commands

import (
	"jdatabase/data"
	"regexp"
)

func newInsert() *insert {
	return &insert{regexp.MustCompile(
			"(?i)insert\\s*(\\((.+)\\))\\s*into\\s+(\\S+)\\s*;"), nil}
}

type insert struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *insert) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd insert) Execute() error {
	return data.GetTC().Insert(cmd.matchedInput[0][3], cmd.matchedInput[0][2])
}
