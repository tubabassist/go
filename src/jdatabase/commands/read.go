package commands

import (
	"os"
	"regexp"
)

func newRead() *read {
	return &read{regexp.MustCompile("(?i)read\\s+'(.+)'\\s*;"), nil}
}

type read struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *read) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd read) Execute() error {
	if file, err := os.Open(cmd.matchedInput[0][1]); err != nil {
		return err
	} else {
		return Process(file)
	}
	return nil
}
