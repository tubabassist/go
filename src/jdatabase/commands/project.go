package commands

import (
	"fmt"
	"regexp"
)

func newProject() *project {
	return &project{
		regexp.MustCompile("(?i)project\\s+(\\(.+\\)|\\S+)\\s+over\\s+(.+)\\s*;"), nil}
}

type project struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *project) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd project) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct PROJECT command!")
	return nil
}
