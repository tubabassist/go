package commands

import (
	"fmt"
	"regexp"
)

func newDefineIndex() *defineIndex {
	return &defineIndex{
		regexp.MustCompile("(?i)define\\s+index\\s+on\\s+(\\S+)\\s*(\\((\\S+)\\))\\s*;"), nil}
}

type defineIndex struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *defineIndex) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd defineIndex) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][3])
	fmt.Println("This is a correct DEFINE_INDEX command!")
	return nil
}
