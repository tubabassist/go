package commands

import (
	"jdatabase/data"
	"regexp"
)

func newDefineTable() *defineTable {
	return &defineTable{
		regexp.MustCompile(
			"(?i)define\\s+table\\s+(\\S+)\\s+having\\s+fields\\s*(\\((.+)\\))\\s*;"), nil}
}

type defineTable struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *defineTable) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd defineTable) Execute() error {
	if err := data.GetTC().DefineTable(cmd.matchedInput[0][1], cmd.matchedInput[0][3]); err != nil {
		return err
	}
	return data.GetTC().WriteToFile();
}
