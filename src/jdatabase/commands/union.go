package commands

import (
	"fmt"
	"regexp"
)

func newUnion() *union {
	return &union{
		regexp.MustCompile("(?i)union\\s+(\\(.+\\)|\\S+)\\s+and\\s+(\\(.+\\)|\\S+)\\s*;"), nil}
}

type union struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *union) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd union) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct UNION command!")
	return nil
}
