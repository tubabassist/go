package commands

import (
	"fmt"
	"regexp"
)

func newMinus() *minus {
	return &minus{
		regexp.MustCompile(
			"(?i)minus\\s+(\\(.+\\)|\\S+)\\s+and\\s+(\\(.+\\)|\\S+)\\s*;"), nil}
}

type minus struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *minus) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd minus) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	fmt.Println("This is a correct MINUS command!")
	return nil
}
