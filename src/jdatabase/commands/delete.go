package commands

import (
	"fmt"
	"regexp"
)

func newDelete() *deleteCommand {
	return &deleteCommand{
		regexp.MustCompile("(?i)delete\\s+(\\S+)(?:\\s+where\\s*(.+))?\\s*;"), nil}
}

type deleteCommand struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *deleteCommand) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd deleteCommand) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	if cmd.matchedInput[0][2] != "" {
		fmt.Println(cmd.matchedInput[0][2])
	}
	fmt.Println("This is a correct DELETE command!")
	return nil
}
