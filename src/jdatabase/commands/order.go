package commands

import (
	"fmt"
	"regexp"
)

func newOrder() *order {
	return &order{
		regexp.MustCompile(
			"(?i)order\\s+(\\(.+\\)|\\S+)\\s+by\\s+(\\S+)\\s*(?:(decending))?\\s*;"), nil}
}

type order struct {
	syntax       *regexp.Regexp
	matchedInput [][]string
}

func (cmd *order) Matches(input string) bool {
	if cmd.syntax.MatchString(input) {
		cmd.matchedInput = cmd.syntax.FindAllStringSubmatch(input, -1)
		return true
	}
	return false
}

func (cmd order) Execute() error {
	fmt.Println(cmd.matchedInput[0][1])
	fmt.Println(cmd.matchedInput[0][2])
	if cmd.matchedInput[0][3] != "" {
		fmt.Println(cmd.matchedInput[0][3])
	}
	fmt.Println("This is a correct ORDER_BY command!")
	return nil
}
