package main

import (
	"fmt"
	"jdatabase/commands"
	"jdatabase/data"
	"os"
)

func main() {
	fmt.Println("<-- Go Database -->", "\nType 'help;' for proper syntax.")
	if err := data.GetTC().ReadFromFile(); err != nil {
		fmt.Println(err)
	}
	for {
		if err := commands.Process(os.Stdin); err != nil {
			fmt.Println(err)
		}
	}
}
